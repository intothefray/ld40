/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour {
	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "Lamp";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion
	private Vector3 target;
	public float heatPerSecond=5f;
	private void OnTriggerStay(Collider other)
	{
		Egg curEgg = other.gameObject.GetComponent<Egg>() as Egg;
		if (curEgg != null){
			if (curEgg.eggState != EggState.Bad)
			{
				curEgg.Heat(heatPerSecond);
			}
		}
	}
}