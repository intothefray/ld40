/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffect : MonoBehaviour {
	public AudioClip[] soundToPlay;
	private AudioSource audioSource;
	private bool startPlaying=false;
	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "SoundEffect";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Update()
	{
		if (!audioSource.isPlaying&&startPlaying==true)
		{
			Destroy(this.gameObject, .2f);
		}else if (!audioSource.isPlaying && startPlaying == false)
		{
			startPlaying = true;
			audioSource.clip = GetRandomSound();
			audioSource.Play();
		}
	}
	private AudioClip GetRandomSound()
	{
		AudioClip sound = soundToPlay[Random.Range(0, soundToPlay.Length)];
		return sound;
	}
}