/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	public GameObject[] prefabsToSpawn;
	public Transform boundry1;
	public Transform boundry2;
	public GameObject soundmaker;
	

	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "Spawner";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private Vector3 GetNewSpawn()
	{
		Vector3 spawn;
		float newX;
		float newY;
		float newZ;

		newX = Random.Range(boundry1.position.x, boundry2.position.x);
		newY = Random.Range(boundry1.position.y, boundry2.position.y);
		newZ = Random.Range(boundry1.position.z, boundry2.position.z);

		spawn = new Vector3(newX, newY, newZ);

		return spawn;
	}

	public void SpawnNewRan()
	{
		Instantiate(prefabsToSpawn[Random.Range(0,prefabsToSpawn.Length)], GetNewSpawn(), new Quaternion(0, 0, 0, 0));
		MakeSoundPackage();
	}

	public void SpawnNew(int index)
	{
		int newIndex = index-1;
		if (index >= prefabsToSpawn.Length)
		{
			newIndex = prefabsToSpawn.Length - 1;
		}
		Instantiate(prefabsToSpawn[newIndex], GetNewSpawn(), new Quaternion(0, 0, 0, 0));
		MakeSoundPackage();
	}

	private void MakeSoundPackage()
	{
		if (soundmaker != null)
		{
			Instantiate(soundmaker, transform.position, transform.rotation);
		}
	}
}