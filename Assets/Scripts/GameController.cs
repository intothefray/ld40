/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	public string startMenu = "StartPage";
	public string MainGame = "MainStage";
	public string GameOver = "Gameover";
	public string tutorial = "Tutorial";
	#region Debug
	public bool isDebug = false;
	private string debugScriptName = "SceneHandler";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	public void GoStart()
	{
		SceneManager.LoadScene(startMenu);
	}

	public void GoGame()
	{
		SceneManager.LoadScene(MainGame);
	}

	public void GoGameover()
	{
		SceneManager.LoadScene(GameOver);
	}

	public void GoTutorial()
	{
		SceneManager.LoadScene(tutorial);
	}

	public float GetHighScore()
	{
		return PlayerPrefs.GetFloat("HighScore");
	}

	public void EndGame()
	{
		Application.Quit();
	}

	private void Update()
	{
		if (Input.GetKeyDown("escape"))
		{
			
		}
	}
}