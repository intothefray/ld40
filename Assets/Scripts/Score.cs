/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {
	[Header("Display Items")]
	private string scoreText;
	public Text scoreDisplay;
	public Slider badEggSlider;
	public Image badEggFill;
	public Image badEggIcon;
	[Header("Display Settings")]
	public Color32 healthyColor;
	public Color32 spoiledColor;
	private Color32 currentColor;
	public Sprite goodEgg;
	public Sprite CrackedEgg;
	public Sprite brokenEgg;
	[Header("Values")]
	private int badEggs;
	public int numBadEggsToFail = 100;
	[SerializeField]
	private float score = 0;
	private bool lastBadEggStage=false;
	[Header("System Variabels")]
	public string playerScore = "PlayerScore";
	public string GameOverScene = "Gameover";

	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "Score";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion
	private void Start()
	{
		PlayerPrefs.SetFloat(playerScore, 0);
		scoreText = score.ToString();
		scoreDisplay.text = scoreText;
		UpdateBadEggCounter();
	}

	public void AddScore(float increment)
	{
		score = score+increment;
		scoreText = score.ToString();
		scoreDisplay.text = scoreText;
	}

	private void UpdateBadEggCounter()
	{
		badEggSlider.value = badEggs;
		ChangeColor();
		ChangeIcon();
	}

	private void ChangeColor()
	{
		currentColor = Color32.Lerp(healthyColor, spoiledColor,(badEggs * .01f));
		badEggIcon.color = currentColor;
		badEggFill.color = currentColor;
	}

	private void ChangeIcon()
	{
		if (badEggs < 33)
		{
			badEggIcon.sprite = goodEgg;
		}
		else if(badEggs < 66)
		{
			badEggIcon.sprite = CrackedEgg;
		}
		else
		{
			badEggIcon.sprite = brokenEgg;
			if (!lastBadEggStage)
			{
				lastBadEggStage = true;
			}
		}
	}

	public void AddBadEgg()
	{
		badEggs++;
		UpdateBadEggCounter();
		if (badEggs >= numBadEggsToFail)
		{
			GameOver();
		}
	}

	private void GameOver()
	{
		PlayerPrefs.SetFloat(playerScore, score);
		SceneManager.LoadScene(GameOverScene);
	}
}