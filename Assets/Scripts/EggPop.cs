/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggPop : MonoBehaviour {
	public GameObject topEgg;
	public GameObject baseEgg;

	private float timePast;
	public float topLifeTime=2f;
	public float botLifeTime=2.5f;
	private float thisTime;
	public float force=20f;
	private bool applyForce=true;
	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "EggHatch";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private void Start()
	{
		if (topLifeTime < botLifeTime)
		{
			thisTime = botLifeTime + .1f;
		}
		else
		{
			thisTime = topLifeTime + .1f;
		}
	}

	private void Awake()
	{
		
	}

	private void FixedUpdate()
	{
		if (applyForce)
		{
			topEgg.GetComponent<Rigidbody>().AddForce(1f,200f,.2f);
			applyForce = false;
		}
	}

	private void Update()
	{
		timePast += Time.deltaTime;
		if (thisTime >= timePast)
		{
			if (botLifeTime < timePast)
			{
				Destroy(baseEgg.gameObject);
			}
			if (topLifeTime < timePast)
			{
				Destroy(topEgg.gameObject);
			}
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
}