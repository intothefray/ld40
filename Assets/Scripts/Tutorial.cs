/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;


[Serializable]
public class TStep
{
	public GameObject[] objects;


	public void ObjectShow()
	{
		foreach(GameObject obj in objects)
		{
			obj.GetComponent<Image>().enabled = true;
		}
	}

	public void ObjectHide()
	{
		foreach (GameObject obj in objects)
		{
			obj.GetComponent<Image>().enabled = false;
		}
	}
}

	public class Tutorial : MonoBehaviour {

	[Header("Properties")]
	public int step = 1;
	public string nextScene = "MainStage";
	private int lastStep;
	public SoundEffect sound;

	[Header("Steps")]
	public TStep[] steps;
	private SceneManager sm;

	private void Start()
	{
		step = -1;
		foreach(TStep step in steps)
		{
			step.ObjectHide();
		}
		lastStep = steps.Length;
		NextStep();
	}

	public void NextStep()
	{
		if (step >= 0)
		{
			Instantiate(sound, transform.position, transform.rotation);
			steps[step].ObjectHide();
		}
		step++;
		if (step == lastStep)
		{
			Done();
			return;
		}
		steps[step].ObjectShow();
	}

	public void Done()
	{
		SceneManager.LoadScene(nextScene);
	}
}