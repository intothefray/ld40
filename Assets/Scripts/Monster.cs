/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {
	public Spawner hatchedEggSpawner;
	public string hatchedEggSpawnerTag = "HatchedEggSpawner";
	public float timeBetweenEggs = 5;
	private float lastEgg;
	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "Monster";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private void Start()
	{
		hatchedEggSpawner = GameObject.FindGameObjectWithTag(hatchedEggSpawnerTag).GetComponent<Spawner>();
		lastEgg = 0;
	}

	private void PassTime()
	{
		lastEgg += Time.deltaTime;
	}

	private void Update()
	{
		if (lastEgg >= timeBetweenEggs)
		{
			hatchedEggSpawner.SpawnNewRan();
			lastEgg = 0;
		}
		PassTime();
	}
}