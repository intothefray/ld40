/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget : MonoBehaviour {
	public Vector3 target;
	public float moveSpeed=5;
	public float stopdistance=.3f;

	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "MoveToTarget";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private void Start()
	{
		target = transform.position;
	}

	public void NewTarget(Vector3 newTar)
	{
		target = newTar;
	}

	public void Move()
	{
		transform.position = Vector3.Lerp(transform.position, target,moveSpeed * Time.deltaTime);
	}

	private void Update()
	{
			Move();
	}

	public void MoveOnX(float xValue)
	{
		target = new Vector3(xValue, transform.position.y, transform.position.z);
	}
}