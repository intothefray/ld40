/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenClick : MonoBehaviour {
	public LayerMask layerMask;
	public LayerMask lightMove;
	public string lampTag = "Lamp";
	public string eggTag = "Egg";
	GameObject lamp;

	public bool holdingObject = false;

	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "ScreenClick";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private void Start()
	{
		lamp = GameObject.FindGameObjectWithTag(lampTag);
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, 300, layerMask))
			{
				GameObject hitObject = hit.collider.gameObject;
				if (hitObject.tag == lampTag)
				{
					holdingObject = true;
				}else if(hitObject.tag == eggTag)
				{
					hitObject.GetComponent<Egg>().EggClicked();
					return;
				}
				
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			holdingObject = false;
		}

		if (holdingObject)
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray, out hit, 300, lightMove)){
				lamp.GetComponent<MoveToTarget>().MoveOnX(hit.point.x);
			}
		}else if (Input.GetMouseButton(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit, 300, layerMask))
			{
				GameObject hitObject = hit.collider.gameObject;
				if (hitObject.tag == eggTag)
				{
					hitObject.GetComponent<Egg>().EggClicked();
					return;
				}

			}
		}
	}
}