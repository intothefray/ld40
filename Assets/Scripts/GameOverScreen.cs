/* Project: 
 * Date: 
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:
 * 
 * Log:
 * Date:	Rev 0
 * Description
 * Inishal Creation.
 * 
 */
using UnityEngine;
using TMPro;

public class GameOverScreen : MonoBehaviour {
	public TextMeshProUGUI highScore;
	public TextMeshProUGUI lastScore;
	public TextMeshProUGUI message;

	public string[] highScoreMessages;
	public string[] medScoreMessages;
	public string[] lowScoreMessages;

	public float highScoreVal;
	public float playerScore;

	

	#region Debug
	public bool isDebug=true;
	private string debugScriptName = "GameOverScreen";

	private void PrintDebugMsg(string msg)
	{
		if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintWarningDebugMsg(string msg)
	{
		Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	private void PrintErrorDebugMsg(string msg)
	{
		Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
	}
	#endregion

	private void Start()
	{
		if (!PlayerPrefs.HasKey("Highscore"))
		{
			PlayerPrefs.SetFloat("Highscore", 0);
		}
		if (!PlayerPrefs.HasKey("PlayerScore"))
		{
			PlayerPrefs.SetFloat("PlayerScore", 0);
		}

		highScoreVal = PlayerPrefs.GetFloat("Highscore");
		playerScore = PlayerPrefs.GetFloat("PlayerScore");
		highscore();
	}

	

	void highscore()
	{
		if (playerScore > highScoreVal)
		{
			PlayerPrefs.SetFloat("Highscore",playerScore);
			message.text = highScoreMessages[Random.Range(0, highScoreMessages.Length)];

		}else if (playerScore == highScoreVal)
		{
			message.text = "Wow! you got the exact same score! Nice Hax Bro ;)";
		}else if (playerScore > (highScoreVal * .70f))
		{
			message.text = medScoreMessages[Random.Range(0, medScoreMessages.Length)];
		}
		else
		{
			message.text = lowScoreMessages[Random.Range(0, lowScoreMessages.Length)];
		}
		highScoreVal = PlayerPrefs.GetFloat("Highscore");
		highScore.text = highScoreVal.ToString();
		lastScore.text = playerScore.ToString();
	}

	private void Update()
	{
		if (Input.GetKeyDown("escape"))
		{
			Application.Quit();
		}
	}
}