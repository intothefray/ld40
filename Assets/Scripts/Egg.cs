/* Project: LD40Egg
 * Date: 12/2/2017
 * Programmer: Aaron Effinger
 * Rev:0
 * 
 * Description:An Egg
 * 
 * Proporties:
 * 
 * Methods:
 * 
 * Log:
 * Date:			Rev		Description
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour {
	[Header("Properties")]
	public float heat;
	public float coolRate;
	public float pointValue = 20f;
	public EggState eggState = EggState.Good;
	public float badEggDestroyValue = -50f;
	[Header("Sprites")]
	public Color32 healthyColor;
	public Color32 spoiledColor;
	private Color32 currentColor;
	public Sprite goodEgg;
	public Sprite badEgg;
	public Sprite hatchEgg;
	Score score;
	private SpriteRenderer currentImage;
	[Header("Spawners")]
	public string eggSpawnerTag = "NewEggSpawner";
	public string monsterSpawnerTag = "MonsterSpawner";
	public Spawner monsterSpawner;
	public Spawner eggSpawner;
	public GameObject hatchedEggPrefab;
	[Header("Sounds")]
	public GameObject badEggSound;
	private bool badEggSoundPlayed = false;

	

	private void Start()
	{
		monsterSpawner = GameObject.FindGameObjectWithTag(monsterSpawnerTag).GetComponent<Spawner>();
		eggSpawner = GameObject.FindGameObjectWithTag(eggSpawnerTag).GetComponent<Spawner>();
		currentImage = GetComponent<SpriteRenderer>();
		score = Camera.main.gameObject.GetComponent<Score>();
		//audioSource = GetComponent<AudioSource>();
	}

	public void Heat(float inHeat)
	{
		heat += inHeat;
	}

	private void Update()
	{
		heat -= coolRate*Time.deltaTime;
		ChangeColor();
		CheckEgg();
		if (eggState == EggState.Hatchable)
		{
			EggClicked();
		}
	}

	private void ChangeColor()
	{
		currentColor = Color32.Lerp(spoiledColor, healthyColor,(heat *.02f));
		currentImage.material.SetColor("_Color",currentColor);
	}

	private void CheckEgg()
	{
		if (heat >= 100)
		{
			eggState = EggState.Hatchable;
		}
		if (eggState != EggState.Hatchable)
		{
			if (heat <= 0)
			{
				eggState = EggState.Bad;
			}
		}
		ChangeEgg();
		if (eggState == EggState.Bad && heat < badEggDestroyValue)
		{
			EggClicked();
		}
	}

	private void ChangeEgg()
	{
		switch (eggState)
		{
			case EggState.Bad:
				currentImage.sprite = badEgg;
				if (!badEggSoundPlayed)
				{
					//score.AddScore(-pointValue);
					score.AddBadEgg();
					Instantiate(badEggSound, transform.position, transform.rotation);
					badEggSoundPlayed = true;
				}
				break;
			case EggState.Good:
				currentImage.sprite = goodEgg;
				break;
			case EggState.Hatchable:
				currentImage.sprite = hatchEgg;
				break;
			case EggState.New:
				currentImage.sprite = goodEgg;
				break;
			default:
				currentImage.sprite = goodEgg;
				break;
		}
	}

	public void EggClicked()
	{
		switch (eggState)
		{
			case EggState.Bad:
				//Destroy(transform.gameObject);
				break;
			case EggState.Good:
				//nothing too do. its all good :)
				break;
			case EggState.New:
				eggSpawner.SpawnNewRan();
				Destroy(transform.gameObject);
				break;
			case EggState.Hatchable:
				monsterSpawner.SpawnNewRan();
				score.AddScore(pointValue);
				Instantiate(hatchedEggPrefab, transform.position, transform.rotation);
				Destroy(transform.gameObject);
				break;
			default:
				
				break;
		}
	}
}

public enum EggState { Good,Hatchable,Bad,New};
